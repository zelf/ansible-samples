# Ubuntu install ansible

```sh
sudo apt-add-repository ppa:ansible/ansible
sudo apt update
sudo apt install ansible
```

# Inventory

update `inventory.ini` with the list of all the VMs

# ping

To ping all servers run: 

```sh
ansible-playbook -i inventory.ini ping_servers.yml
```

# update

to update all the server in the updatable list:

```sh
ansible-playbook -i inventory.ini update_and_reboot.yml
```

# Install Wazuh agents

```sh
ansible-playbook -i inventory.ini install_wazuh_agent.yml
```
